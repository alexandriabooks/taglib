import typing

import taglib

from alexandriabooks_core import model
from alexandriabooks_core.services import BookParserService, registry


class TagLibBookParserService(BookParserService):
    def __init__(self, service_registry: registry.ServiceRegistry, **kwargs):
        self.service_registry: registry.ServiceRegistry = service_registry

    def __str__(self):
        return "TagLib Audio Book Parser"

    async def can_parse(self, accessor: model.FileAccessor) -> bool:
        """
        Checks to see if the service can read this source type.
        :param accessor: The accessor.
        :return: True if it could be read, False otherwise.
        """
        local_file = accessor.get_local_path()
        if local_file is None:
            return False

        source_type = model.SourceType.find_source_type(local_file)
        return source_type in [
            model.SourceType.FLAC,
            model.SourceType.M4A,
            model.SourceType.MP3,
            model.SourceType.OGG,
            model.SourceType.WAV,
            model.SourceType.WMA
        ]

    async def parse_book(self, accessor: model.FileAccessor) -> typing.Optional[dict]:
        """
        Attempt to read the given book.
        :param accessor: The accessor to read the book.
        :return: The book as a dictionary, or None if the book cannot be read.
        """
        local_file = accessor.get_local_path()
        if local_file is None:
            return None
        source_type = model.SourceType.find_source_type(local_file)
        if source_type not in [
            model.SourceType.FLAC,
            model.SourceType.M4A,
            model.SourceType.MP3,
            model.SourceType.OGG,
            model.SourceType.WAV,
            model.SourceType.WMA
        ]:
            return None
        tag_file = taglib.File(local_file)
        metadata = {
            "source_type": source_type
        }
        if "ALBUM" in tag_file.tags:
            metadata["title"] = tag_file.tags["ALBUM"][0]
        if "TITLE" in tag_file.tags:
            if "ALBUM" not in tag_file.tags:
                metadata["title"] = tag_file.tags["TITLE"][0]
            else:
                metadata["subtitle"] = tag_file.tags["TITLE"][0]
        if "PUBLISHER" in tag_file.tags:
            metadata["publisher"] = tag_file.tags["PUBLISHER"][0]
        if "DATE" in tag_file.tags:
            metadata["publish_date"] = tag_file.tags["DATE"][0]
        if "ARTIST" in tag_file.tags:
            writers = []
            for artist in tag_file.tags["ARTIST"]:
                person_id = artist.lower()
                person_id = await self.service_registry.add_person_source(
                    model.SourceProvidedPersonMetadata(
                        source_key=person_id,
                        name=artist
                    ),
                    person_id=person_id
                )
                writers.append(person_id)
            metadata["writers"] = writers
        if "GENRE" in tag_file.tags:
            metadata["genres"] = tag_file.tags["GENRE"]
        return metadata

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        return []
