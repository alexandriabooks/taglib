import typing
import logging

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}

try:
    from .booktypes import TagLibBookParserService

    ALEXANDRIA_PLUGINS["BookParserService"] = [TagLibBookParserService]
except ImportError as e:
    logger.debug("Cannot load taglib plugin", exc_info=e)
